extern crate gdk;
extern crate glib;
extern crate gtk;
extern crate webkit2gtk;

use std::env;

use glib::ToVariant;
use gtk::{ContainerExt, Inhibit, WidgetExt, Window, WindowType};
use webkit2gtk::{SettingsExt, WebContext, WebContextExt, WebView, WebViewExt};
use gtk::GtkWindowExt;
use gdk::WindowTypeHint;

fn main() {
    let args: Vec<String> = env::args().collect();
    if args.len() < 2 {
        println!("usage: {} <url>", args[0]);
        return;
    }

    let url = &args[1];

    gtk::init().unwrap();

    /* create a window that acts as the new desktop */
    let window = Window::new(WindowType::Toplevel);
    window.set_decorated(false);
    window.set_keep_below(true);
    window.set_skip_pager_hint(true);
    window.set_skip_taskbar_hint(true);
    window.stick();
    window.set_type_hint(WindowTypeHint::Desktop);
    window.fullscreen();
    window.maximize(); // ^ above works, but this not

    let screen: gdk::Screen = window.get_screen().unwrap();

    /* set the visual to rgba for transparency */
    let vis = screen.get_rgba_visual().unwrap();
    window.set_visual(Some(&vis));

    let context = WebContext::get_default().unwrap();
    context.set_web_extensions_initialization_user_data(&"webkit".to_variant());
    context.set_web_extensions_directory("../webkit2gtk-webextension-rs/example/target/debug/");
    let webview = WebView::new_with_context(&context);

    /* actually setup transparency for webkit */
    webview.set_background_color(&gdk::RGBA {
        red: 0.0,
        green: 0.0,
        blue: 0.0,
        alpha: 0.0,
    });

    webview.load_uri(url);
    window.add(&webview);

    /* enable the inspector, usefull for debugging */
    let settings = WebViewExt::get_settings(&webview).unwrap();
    settings.set_enable_developer_extras(true);

    window.show_all();

    window.connect_delete_event(|_, _| {
        gtk::main_quit();
        Inhibit(false)
    });

    gtk::main();
}
